package com.dksd.whiteelephant;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NftControllerTest {

    private NftController nftController;

    @BeforeEach
    void setUp() {
        nftController = new NftController();
        nftController.createWhiteElephant("Appen NFT White Elephant 2021", "description");
    }

    @Test
    void getWhiteElephantTest() {
        System.out.println(nftController.getWhiteElephant());
    }

    @Test
    void smallCreateAndStealTest() throws OptimisticLockException {
        String robsNftId = UUID.randomUUID().toString();
        System.out.println(nftController.createNFTGift(robsNftId, "Robs Picture of the Fonz",
                "fonz desc", "Rob", "picture"));
        WhiteElephant whiteElephant = nftController.getWhiteElephant().getBody();
        //Do some verifications
        assertTrue(whiteElephant.getCheerForNftId(robsNftId).getOwner().equalsIgnoreCase("Rob"));
        assertTrue(whiteElephant.getCheerForNftId(robsNftId).getParentId().equalsIgnoreCase(""));
        System.out.println(nftController.setPersonTurn("Rob"));
        nftController.stealNFTGift(robsNftId, "Dilbert");
        whiteElephant = nftController.getWhiteElephant().getBody();
        assertTrue(whiteElephant.getCheerForNftId(robsNftId).getOwner().equalsIgnoreCase("Dilbert"));
        assertFalse(whiteElephant.getCheerForNftId(robsNftId).getParentId().equalsIgnoreCase(""));
    }

    @Test
    void fullPartySimTest() throws OptimisticLockException {
        String RobsNftId = UUID.randomUUID().toString();
        String magsNftId = UUID.randomUUID().toString();
        String dilbertNftId = UUID.randomUUID().toString();
        System.out.println(nftController.createNFTGift(RobsNftId, "Robs Picture of the Fonz",
                "fonz desc", "Rob", "picture"));
        System.out.println(nftController.createNFTGift(RobsNftId, "Dilbert Picture of a parking lot",
                "parking lot", "Dilbert", "picture"));
        System.out.println(nftController.createNFTGift(RobsNftId, "Mags first commit sha",
                "first sha", "Mage", "sha1"));
        WhiteElephant whiteElephant = nftController.getWhiteElephant().getBody();
        //Do some verifications
        System.out.println(nftController.setPersonTurn("Rob"));
        nftController.stealNFTGift(RobsNftId, "Dilbert");
        nftController.stealNFTGift(RobsNftId, "Mage");
        System.out.println(nftController.setPersonTurn("Mage"));
        nftController.stealNFTGift(RobsNftId, "Dilbert");
        nftController.stealNFTGift(RobsNftId, "Rob");
        System.out.println(nftController.setPersonTurn("Dilbert"));
        nftController.stealNFTGift(RobsNftId, "Rob");
        nftController.stealNFTGift(RobsNftId, "Mage");
        System.out.println(nftController.setPersonTurn("Rob"));
        nftController.stealNFTGift(RobsNftId, "Dilbert");
        nftController.stealNFTGift(RobsNftId, "Mage");
        whiteElephant = nftController.getWhiteElephant().getBody();
        System.out.println(whiteElephant);
    }
}