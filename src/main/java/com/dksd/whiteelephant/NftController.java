package com.dksd.whiteelephant;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

@Controller
@RequestMapping(value = "/whiteElephant")
public final class NftController {
    private final Logger logger = Logger.getLogger(getClass().getName());
    private int cheerLengthToMatch = 3; //Character count to match
    private volatile String cheerHashTarget = getTarget();//The problem to solve
    private WhiteElephant whiteElephant;

    @PutMapping
    public synchronized ResponseEntity<WhiteElephant> createWhiteElephant(
            @RequestParam(name = "name") String whiteElephantName,
            @RequestParam(name = "description") String description) {
        whiteElephant = new WhiteElephant(whiteElephantName, description);
        return new ResponseEntity<>(whiteElephant, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<WhiteElephant> getWhiteElephant() {
        return new ResponseEntity<>(whiteElephant, HttpStatus.OK);
    }

    @PutMapping(value = "/createNFTGift")
    public synchronized ResponseEntity<WhiteElephant> createNFTGift(
            @RequestParam(name = "nft_id") String nftId,
            @RequestParam(name = "nft_name") String nftName,
            @RequestParam(name = "nft_description") String description,
            @RequestParam(name = "owner") String owner,
            @RequestBody String base64NFT) throws OptimisticLockException {
        String parentCheer = getLatestRejoicementId();
        Cheer mintedCheer = createNewCheer(new ProposedCheer(parentCheer, base64NFT, owner), cheerLengthToMatch, cheerHashTarget);
        mintedCheer.setNftId(nftId);
        mintedCheer.setNftName(nftName);
        mintedCheer.setNftDescription(description);
        mintedCheer.setOwner(owner);
        mintedCheer.setBase64Data(base64NFT);
        mintedCheer.setParentId(parentCheer);
        logger.info("Freshly minted NFT Cheer by owner:  " + mintedCheer + "," + owner);
        return new ResponseEntity<>(whiteElephant, HttpStatus.OK);
    }

    @PutMapping(value = "/stealNFTGift")
    public ResponseEntity<WhiteElephant> stealNFTGift(
            @RequestParam(name = "nft_id") String nftId,
            @RequestParam(name = "new_owner") String new_owner) throws OptimisticLockException {
        String owner = whiteElephant.getOwner(nftId);
        if (!whiteElephant.getTurnName().equalsIgnoreCase(owner)
                || whiteElephant.getCheerForNftId(nftId).getSteals() >= 3) {
            whiteElephant.setError("Cannot steal this NFT: " + nftId + " attempted steal by: " + new_owner + " on owner: " + owner);
            return new ResponseEntity<>(whiteElephant, HttpStatus.BAD_REQUEST);
        }
        String latestCheer = getLatestRejoicementId();
        Cheer nftCheer = whiteElephant.getCheerForNftId(nftId);
        Cheer changeOwner = createNewCheer(new ProposedCheer(latestCheer,
                nftCheer.getBase64Data(), new_owner), cheerLengthToMatch, cheerHashTarget);
        changeOwner.setSteals(nftCheer.getSteals() + 1);
        changeOwner.setParentId(latestCheer);
        changeOwner.setBase64Data(nftCheer.getBase64Data());
        changeOwner.setOwner(new_owner);
        changeOwner.setNftId(nftId);
        changeOwner.setNftName(nftCheer.getNftName());
        changeOwner.setNftDescription(nftCheer.getNftDescription());
        return new ResponseEntity<>(whiteElephant, HttpStatus.OK);
    }

    @PutMapping(value = "/setPersonsTurn")
    public synchronized ResponseEntity<WhiteElephant> setPersonTurn(
            @RequestParam(name = "name") String whiteElephantParticipant) {
        this.whiteElephant.setTurnName(whiteElephantParticipant);
        this.whiteElephant.setRound(whiteElephant.getRound() + 1);
        return new ResponseEntity<>(whiteElephant, HttpStatus.OK);
    }

    private String getLatestRejoicementId() {
        if (whiteElephant.isEmpty()) return "";
        return whiteElephant.getLast().getBlockId();
    }

    private void signCheer(Cheer mintedCheer) {
        try {
            mintedCheer.setSignature(Base64.getEncoder().encodeToString(
                    new SignatureVerification().sign(
                            mintedCheer.toString().getBytes(StandardCharsets.UTF_8))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getTarget() {
        String newHash = hash(UUID.randomUUID().toString(),
                new Random().nextInt(Integer.MAX_VALUE)).substring(0, cheerLengthToMatch);
        logger.info("Setting new target hash to solve: " + newHash);
        return newHash;
    }

    private String hash(String data, int nonce) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(data.getBytes());
        md.update(("" + nonce).getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }

    private Cheer createNewCheer(ProposedCheer cheer, int target_len, String targetHash) throws OptimisticLockException {
        logger.info("Starting to mine block: " + cheer.toString() + " for whiteElephant: " + whiteElephant.toString());
        int nonce = 0;
        while (true) {
            String guess = hash(cheer.toString(), nonce);
            if (guess.substring(0, target_len).equals(targetHash)) {
                Cheer newCheer = new Cheer();
                newCheer.setBlockId(guess);
                newCheer.setNonce(""+nonce);
                synchronized (whiteElephant) {
                    if (whiteElephant.getLast().getBlockId().equalsIgnoreCase(cheer.getParentId())) {
                        whiteElephant.addLast(newCheer);
                        cheerHashTarget = getTarget();//set a new target
                        signCheer(newCheer);
                    } else {
                        throw new OptimisticLockException("The chain of cheer has been extra cheerie, and you your cheer needs tobe retried.");
                    }
                }
                return newCheer;
            }
            nonce++;
        }
    }
}
