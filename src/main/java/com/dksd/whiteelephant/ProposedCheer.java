package com.dksd.whiteelephant;

public class ProposedCheer {
    String parentId;
    String blockId;
    String base64Data;
    String owner;

    public ProposedCheer(String parentId,
                         String base64Data,
                         String owner) {
        this.parentId = parentId;
        this.base64Data = base64Data;
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Block{" +
                "parentId='" + parentId + '\'' +
                ", blockId='" + blockId + '\'' +
                ", base64Data='" + base64Data + '\'' +
                ", owner='" + owner + '\'' +
                '}';
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public String getBase64Data() {
        return base64Data;
    }

    public void setBase64Data(String base64Data) {
        this.base64Data = base64Data;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

}
