package com.dksd.whiteelephant;

public class Cheer {
    String parentId;
    String blockId;
    String nftId;
    String base64Data;
    String owner;
    String nonce;

    public String getNftId() {
        return nftId;
    }

    public void setNftId(String nftId) {
        this.nftId = nftId;
    }

    int steals;
    String nftName;
    String nftDescription;
    String signature;

    public Cheer() {

    }

    public Cheer(String parentId,
                 String nftId,
                 String base64Data,
                 String owner,
                 String nftName,
                 String nftDescription) {
        this.parentId = parentId;
        this.base64Data = base64Data;
        this.owner = owner;
        this.nftName = nftName;
        this.nftDescription = nftDescription;
    }

    @Override
    public String toString() {
        return "Cheer{" +
                "parentId='" + parentId + '\'' +
                ", blockId='" + blockId + '\'' +
                ", base64Data='" + base64Data + '\'' +
                ", owner='" + owner + '\'' +
                ", nonce='" + nonce + '\'' +
                ", steals=" + steals +
                ", nftName='" + nftName + '\'' +
                ", nftDescription='" + nftDescription + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public String getBase64Data() {
        return base64Data;
    }

    public void setBase64Data(String base64Data) {
        this.base64Data = base64Data;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
    public int getSteals() {
        return steals;
    }

    public void setSteals(int steals) {
        this.steals = steals;
    }

    public String getNftName() {
        return nftName;
    }

    public void setNftName(String nftName) {
        this.nftName = nftName;
    }

    public String getNftDescription() {
        return nftDescription;
    }

    public void setNftDescription(String nftDescription) {
        this.nftDescription = nftDescription;
    }
}
