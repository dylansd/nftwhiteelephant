package com.dksd.whiteelephant;

public class OptimisticLockException extends Throwable {
    public OptimisticLockException(String msg) {
        super(msg);
    }
}
