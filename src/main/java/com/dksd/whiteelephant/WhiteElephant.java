package com.dksd.whiteelephant;

import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;

public class WhiteElephant {
    private LinkedBlockingDeque<Cheer> whiteElephant = new LinkedBlockingDeque<>();
    private int round;
    private String whiteElephantDescription;
    private String error;
    private String turnName;
    private Set<String> participants = new HashSet<>();

    public WhiteElephant(String whiteElephantName, String description) {

    }

    public boolean isEmpty() {
        return whiteElephant.isEmpty();
    }

    public Cheer getLast() {
        return whiteElephant.peekLast();
    }

    public void addLast(Cheer mintedCheer) {
        this.participants.add(mintedCheer.getOwner());
        whiteElephant.addLast(mintedCheer);
    }

    public Cheer getCheerForNftId(String nftId) {
        Cheer latestCheer = null;
        for (Cheer cheer : this.whiteElephant) {
            if (cheer.getNftId().equals(nftId)) {
                latestCheer = cheer;
            }
        }
        return latestCheer;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Set<String> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<String> participants) {
        this.participants = participants;
    }

    public String getError() {
        return error;
    }

    public void showOwners() {
        Map<String, String> nftToOwner = new HashMap<>();
        Map<String, String> ownerToNft = new HashMap<>();
        for (Cheer cheer : this.whiteElephant) {
            nftToOwner.put(cheer.getNftId(), cheer.getOwner());
            ownerToNft.put(cheer.getOwner(), cheer.getNftId());
        }
        System.out.println(nftToOwner);
        System.out.println(ownerToNft);
    }

    public String getOwner(String nftId) {
        Map<String, String> nftToOwner = new HashMap<>();
        for (Cheer cheer : this.whiteElephant) {
            nftToOwner.put(cheer.getNftId(), cheer.getOwner());
        }
        return nftToOwner.get(nftId);
    }
    public LinkedBlockingDeque<Cheer> getWhiteElephant() {
        return whiteElephant;
    }

    public void setWhiteElephant(LinkedBlockingDeque<Cheer> whiteElephant) {
        this.whiteElephant = whiteElephant;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String getWhiteElephantDescription() {
        return whiteElephantDescription;
    }

    public void setWhiteElephantDescription(String whiteElephantDescription) {
        this.whiteElephantDescription = whiteElephantDescription;
    }

    public String getTurnName() {
        return turnName;
    }

    public void setTurnName(String turnName) {
        this.turnName = turnName;
    }


}
