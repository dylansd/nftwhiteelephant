package com.dksd.whiteelephant;

import java.security.*;

public class SignatureVerification {

    // Cool https://www.tutorialspoint.com/java_cryptography/java_cryptography_verifying_signature.htm
    private KeyPairGenerator keyPairGen;
    private KeyPair pair;

    public SignatureVerification() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        setup();
    }

    private void setup() throws NoSuchAlgorithmException {
        keyPairGen = KeyPairGenerator.getInstance("DSA");
        keyPairGen.initialize(2048);
        pair = keyPairGen.generateKeyPair();
    }

    public byte[] sign(byte[] data) throws InvalidKeyException, SignatureException, NoSuchAlgorithmException {
        long st = System.currentTimeMillis();
        Signature sign = Signature.getInstance("SHA256withDSA");
        sign.initSign(pair.getPrivate());
        sign.update(data);
        byte [] ans  = sign.sign();
        return ans;
    }

    public boolean verify(byte[] data, byte[] signature) throws InvalidKeyException, SignatureException, NoSuchAlgorithmException {
        Signature sign = Signature.getInstance("SHA256withDSA");
        sign.initSign(pair.getPrivate());
        sign.initVerify(pair.getPublic());
        sign.update(data);
        return sign.verify(signature);
    }

}
